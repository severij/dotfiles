[[ $- != *i* ]] && return

set -o vi
alias ls='ls --color=auto'
alias sbcl='rlwrap sbcl'

PS1='[\u@\h \W]\$ '


if [ -n $DISPLAY ]; then
    export BROWSER=firefox
else
    export BROWSER=elinks
fi

export PATH=$PATH:/home/severij/.local/bin/
export EDITOR=nvim
